#ifndef CONFIG_H
#define CONFIG_H

const int screenWidth = 540;
const int screenHeight = 600;
enum class TYPE { PLAY, OPTIONS, EXIT, RESUME, GOBACK,CREDITS, VOLUME,LEVEL1,LEVEL2,LEVEL3,LEVEL4,LEVEL5,MORE,LESS };
enum class DIRECTION{FRONT,BACK,LEFT,RIGHT};
#endif
