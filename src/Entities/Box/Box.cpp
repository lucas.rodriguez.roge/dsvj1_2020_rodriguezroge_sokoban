#include "Box.h"
Box::Box(Vector2 pos) {
	_texture = LoadTexture("res/Textures/Enviroment/box.png");
	_boxcollider.x = pos.x;
	_boxcollider.y = pos.y;
	_boxcollider.width = _texture.width;
	_boxcollider.height = _texture.height;
}
Texture2D Box::getTexture() {
	return _texture;
}
Rectangle Box::getBoxCollider() {
	return _boxcollider;
}
Vector2 Box::getPos() {
	return { _boxcollider.x,_boxcollider.y };
}
void Box::setPos(Vector2 pos) {
	_boxcollider.x = pos.x;
	_boxcollider.y = pos.y;
}
void Box::setBoxCollider(Rectangle boxcollider) {
	_boxcollider = boxcollider;
}