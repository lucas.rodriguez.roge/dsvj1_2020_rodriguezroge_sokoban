#pragma once
#ifndef BOX_H
#define BOX_H
#include "raylib.h"
class Box {
private:
	Texture2D _texture;
	Rectangle _boxcollider;
public:
	Box(Vector2 pos);
	Texture2D getTexture();
	Rectangle getBoxCollider();
	Vector2 getPos();
	void setPos(Vector2 pos);
	void setBoxCollider(Rectangle boxcollider);
};
#endif // !BOX_H
