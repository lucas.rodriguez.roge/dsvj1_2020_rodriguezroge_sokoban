#include "Buttons.h"

void Buttons::init(Vector2 pos,TYPE type,Texture2D texture) {
	_textbox = texture;
	_type = type;
	_color = WHITE;
	_boxcollider.x = pos.x;
	_boxcollider.y = pos.y;
	_boxcollider.width = _textbox.width;
	_boxcollider.height = _textbox.height;
}

void Buttons::draw() {
	switch (_type)
	{
	case TYPE::PLAY:
		DrawTexture(_textbox, _boxcollider.x, _boxcollider.y, WHITE);
		DrawText("PLAY", _boxcollider.x+50, _boxcollider.y+20, 40, _color);
		break;
	case TYPE::OPTIONS:
		DrawTexture(_textbox, _boxcollider.x, _boxcollider.y, WHITE);
		DrawText("OPTIONS", _boxcollider.x+25, _boxcollider.y+20, 40, _color);
		break;
	case TYPE::EXIT:
		DrawTexture(_textbox, _boxcollider.x, _boxcollider.y, WHITE);
		DrawText("EXIT", _boxcollider.x + 50, _boxcollider.y + 20, 40, _color);
		break;
	case TYPE::RESUME:
		DrawTexture(_textbox, _boxcollider.x, _boxcollider.y, WHITE);
		DrawText("RESUME", _boxcollider.x + 50, _boxcollider.y + 20, 40, _color);
		break;
	case TYPE::GOBACK:
		DrawTexture(_textbox, _boxcollider.x, _boxcollider.y, WHITE);
		DrawText("GOBACK", _boxcollider.x + 25, _boxcollider.y + 20, 40, _color);
		break;
	case TYPE::VOLUME:
		DrawTexture(_textbox, _boxcollider.x, _boxcollider.y, WHITE);
		DrawText("VOLUME", _boxcollider.x + 25, _boxcollider.y + 20, 40, _color);
		break;
	case TYPE::LEVEL1:
		DrawTexture(_textbox, _boxcollider.x, _boxcollider.y, WHITE);
		DrawText("1", _boxcollider.x + 25, _boxcollider.y + 10, 40, _color);
		break;
	case TYPE::LEVEL2:
		DrawTexture(_textbox, _boxcollider.x, _boxcollider.y, WHITE);
		DrawText("2", _boxcollider.x + 25, _boxcollider.y + 10, 40, _color);
		break;
	case TYPE::LEVEL3:
		DrawTexture(_textbox, _boxcollider.x, _boxcollider.y, WHITE);
		DrawText("3", _boxcollider.x + 25, _boxcollider.y + 10, 40, _color);
		break;
	case TYPE::LEVEL4:
		DrawTexture(_textbox, _boxcollider.x, _boxcollider.y, WHITE);
		DrawText("4", _boxcollider.x + 25, _boxcollider.y + 10, 40, _color);
		break;
	case TYPE::LEVEL5:
		DrawTexture(_textbox, _boxcollider.x, _boxcollider.y, WHITE);
		DrawText("5", _boxcollider.x + 25, _boxcollider.y + 10, 40, _color);
		break;
	case TYPE::MORE:
		DrawTexture(_textbox, _boxcollider.x, _boxcollider.y, WHITE);
		DrawText("+", _boxcollider.x + 25, _boxcollider.y + 10, 40, _color);
		break;
	case TYPE::LESS:
		DrawTexture(_textbox, _boxcollider.x, _boxcollider.y, WHITE);
		DrawText("-", _boxcollider.x + 25, _boxcollider.y + 10, 40, _color);
		break;
	case TYPE::CREDITS:
		DrawTexture(_textbox, _boxcollider.x, _boxcollider.y, WHITE);
		DrawText("CREDITS", _boxcollider.x + 25, _boxcollider.y + 20, 40, _color);
		break;
	default:
		break;
	}
}
void Buttons::buttonManagment() {
	switch (_type)
	{
	case TYPE::PLAY:
		if (CheckCollisionPointRec(GetMousePosition(), _boxcollider)) {
			_color = RED;
			if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
				_color = DARKPURPLE;
		}
		else {
			_color = WHITE;
		}
		break;
	case TYPE::OPTIONS:
		draw();
		if (CheckCollisionPointRec(GetMousePosition(), _boxcollider)) {
			_color = RED;
			if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
				_color = DARKPURPLE;
		}
		else {
			_color = WHITE;
		}
		break;
	case TYPE::EXIT:
		if (CheckCollisionPointRec(GetMousePosition(), _boxcollider)) {
			_color = RED;
			if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
				_color = DARKPURPLE;
		}
		else {
			_color = WHITE;
		}
		break;
	case TYPE::RESUME:
		if (CheckCollisionPointRec(GetMousePosition(), _boxcollider)) {
			_color = RED;
			if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
				_color = DARKPURPLE;
		}
		else {
			_color = WHITE;
		}
		break;
	case TYPE::GOBACK:
		if (CheckCollisionPointRec(GetMousePosition(), _boxcollider)) {
			_color = RED;
			if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
				_color = DARKPURPLE;
		}
		else {
			_color = WHITE;
		}
		break;
	case TYPE::VOLUME:
		if (CheckCollisionPointRec(GetMousePosition(), _boxcollider)) {
			_color = RED;
			if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
				_color = DARKPURPLE;
		}
		else {
			_color = WHITE;
		}
		break;
	case TYPE::LEVEL1:
		if (CheckCollisionPointRec(GetMousePosition(), _boxcollider)) {
			_color = RED;
			if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
				_color = DARKPURPLE;
		}
		else {
			_color = WHITE;
		}
		break;
	case TYPE::LEVEL2:
		if (CheckCollisionPointRec(GetMousePosition(), _boxcollider)) {
			_color = RED;
			if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
				_color = DARKPURPLE;
		}
		else {
			_color = WHITE;
		}
		break;
	case TYPE::LEVEL3:
		if (CheckCollisionPointRec(GetMousePosition(), _boxcollider)) {
			_color = RED;
			if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
				_color = DARKPURPLE;
		}
		else {
			_color = WHITE;
		}
		break;
	case TYPE::LEVEL4:
		if (CheckCollisionPointRec(GetMousePosition(), _boxcollider)) {
			_color = RED;
			if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
				_color = DARKPURPLE;
		}
		else {
			_color = WHITE;
		}
		break;
	case TYPE::LEVEL5:
		if (CheckCollisionPointRec(GetMousePosition(), _boxcollider)) {
			_color = RED;
			if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
				_color = DARKPURPLE;
		}
		else {
			_color = WHITE;
		}
		break;
	case TYPE::MORE:
		if (CheckCollisionPointRec(GetMousePosition(), _boxcollider)) {
			_color = RED;
			if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
				_color = DARKPURPLE;
		}
		else {
			_color = WHITE;
		}
		break;
	case TYPE::LESS:
		if (CheckCollisionPointRec(GetMousePosition(), _boxcollider)) {
			_color = RED;
			if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
				_color = DARKPURPLE;
		}
		else {
			_color = WHITE;
		}
		break;
	case TYPE::CREDITS:
		if (CheckCollisionPointRec(GetMousePosition(), _boxcollider)) {
			_color = RED;
			if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
				_color = DARKPURPLE;
		}
		else {
			_color = WHITE;
		}
		break;
	default:
		break;
	}
}
Texture2D Buttons::getTexture() {
	return _textbox;
}
void Buttons::setTexture(Texture2D texture) {
	_textbox = texture;
}
Rectangle Buttons::getBoxCollider() {
	return _boxcollider;
}
void Buttons::setBoxCollider(Rectangle boxcollider) {
	_boxcollider = boxcollider;
}