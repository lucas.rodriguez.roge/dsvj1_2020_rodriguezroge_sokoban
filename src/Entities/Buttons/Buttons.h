#pragma once
#ifndef BUTTONS_H
#define BUTTONS_H
#include "raylib.h"
#include "../../Config.h"
class Buttons {
private:
	Texture2D _textbox;
	TYPE _type;
	Rectangle _boxcollider;
	Color _color;
public:
	void init(Vector2 pos, TYPE type, Texture2D texture);
	void draw();
	void buttonManagment();
	Texture2D getTexture();
	void setTexture(Texture2D texture);
	Rectangle getBoxCollider();
	void setBoxCollider(Rectangle boxcollider);
};
#endif
