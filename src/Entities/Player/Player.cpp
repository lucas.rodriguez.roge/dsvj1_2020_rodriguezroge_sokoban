#include "Player.h"
Player::Player(Vector2 pos) {
	_playerfront = LoadTexture("res/Textures/Player/playerfront.png");
	_playerback = LoadTexture("res/Textures/Player/playerback.png");
	_playerleft = LoadTexture("res/Textures/Player/playerleft.png");
	_playerright = LoadTexture("res/Textures/Player/playerright.png");
	_boxcollider.x = pos.x;
	_boxcollider.y = pos.y;
	_boxcollider.width = _playerfront.width;
	_boxcollider.height = _playerfront.height;
	_direction = DIRECTION::FRONT;
}
Texture2D Player::getTexturePlayerFront() {
	return _playerfront;
}
Texture2D Player::getTexturePlayerBack() {
	return _playerback;
}
Texture2D Player::getTexturePlayerLeft() {
	return _playerleft;
}
Texture2D Player::getTexturePlayerRight() {
	return _playerright;
}
Rectangle Player::getBoxCollider() {
	return _boxcollider;
}
Vector2 Player::getPos() {
	return { _boxcollider.x,_boxcollider.y };
}
void Player::setPos(Vector2 pos) {
	_boxcollider.x = pos.x;
	_boxcollider.y = pos.y;
}
void Player::setBoxCollider(Rectangle boxcollider) {
	_boxcollider = boxcollider;
}
void Player::draw() {
	switch (_direction)
	{
	case DIRECTION::FRONT:
		DrawTexture(_playerfront, _boxcollider.x, _boxcollider.y, WHITE);
		break;
	case DIRECTION::BACK:
		DrawTexture(_playerback, _boxcollider.x, _boxcollider.y, WHITE);
		break;
	case DIRECTION::LEFT:
		DrawTexture(_playerleft, _boxcollider.x, _boxcollider.y, WHITE);
		break;
	case DIRECTION::RIGHT:
		DrawTexture(_playerright, _boxcollider.x, _boxcollider.y, WHITE);
		break;
	default:
		break;
	}
}
DIRECTION Player::getDirection() {
	return _direction;
}
void Player::setDirection(DIRECTION direction) {
	_direction = direction;
}