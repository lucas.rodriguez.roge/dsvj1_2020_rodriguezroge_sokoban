#pragma once
#ifndef PLAYER_H
#define PLAYER_H
#include "raylib.h"
#include "../../Config.h"
class Player {
private:
	Texture2D _playerfront;
	Texture2D _playerback;
	Texture2D _playerleft;
	Texture2D _playerright;
	Rectangle _boxcollider;
	DIRECTION _direction;
public:
	Player(Vector2 pos);
	Texture2D getTexturePlayerFront();
	Texture2D getTexturePlayerBack();
	Texture2D getTexturePlayerLeft();
	Texture2D getTexturePlayerRight();
	Rectangle getBoxCollider();
	Vector2 getPos();
	DIRECTION getDirection();
	void setDirection(DIRECTION direction);
	void setPos(Vector2 pos);
	void setBoxCollider(Rectangle boxcollider);
	void draw();
};
#endif // !PLAYER_H
