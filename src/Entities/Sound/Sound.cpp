#include "Sound.h"
namespace sound {
	Sound selectfirst = LoadSound("res/Sound/selectfirst.wav");
	Sound blockwrong = LoadSound("res/Sound/wrongselection.wav");
	Sound blockcomplete = LoadSound("res/Sound/selectioncomplete.wav");
	Music musicmenu = LoadMusicStream("res/Sound/mainmenu.mp3");
	Music musicgame = LoadMusicStream("res/Sound/game.mp3");
	Music musicoptions = LoadMusicStream("res/Sound/optionsmenu.mp3");
	Music musicvolume= LoadMusicStream("res/Sound/volumemenu.mp3");
	Music musiclevelselection= LoadMusicStream("res/Sound/levelselection.mp3");
	void initSounds() {
		SetSoundVolume(blockcomplete, 0.5f);
		SetSoundVolume(selectfirst, 0.5f);
		SetSoundVolume(blockwrong, 0.5f);
		SetMusicVolume(musicmenu, 0.5f);
		SetMusicVolume(musicgame, 0.5f);
		SetMusicVolume(musicoptions, 0.5f);
		SetMusicVolume(musicvolume, 0.5f);
		SetMusicVolume(musiclevelselection, 0.5f);
	}
	void modificarVolumen(float auxvolumen[], short input) {
		SetSoundVolume(blockcomplete, auxvolumen[input]);
		SetSoundVolume(selectfirst, auxvolumen[input]);
		SetSoundVolume(blockwrong, auxvolumen[input]);
		SetMusicVolume(musicmenu, auxvolumen[input]);
		SetMusicVolume(musicgame, auxvolumen[input]);
		SetMusicVolume(musicoptions, auxvolumen[input]);
		SetMusicVolume(musicvolume, auxvolumen[input]);
		SetMusicVolume(musiclevelselection, auxvolumen[input]);
	}
}