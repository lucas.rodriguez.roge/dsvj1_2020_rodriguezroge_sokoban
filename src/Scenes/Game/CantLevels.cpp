#include "CantLevels.h"
void Level1(tile level1[][cantmaxx]) {
	for (short i = 0; i < cantmaxy; i++) {
		for (short j = 0; j < cantmaxx; j++) {
			if ((i == 1 && j == 1)||(i==cantmaxy-2&&j==cantmaxx-2)) {
				level1[i][j].type = TILE::GROUNDBOX;
			}
			else if ((i==2&&j==2)||(i==cantmaxy-3&&j== cantmaxx - 3)) {
				level1[i][j].type = TILE::BOX;
			}
			else if ((i == 0||i==cantmaxy-1)||(j==0||j==cantmaxx-1)) {
				level1[i][j].type = TILE::WALL;
			}
			else {
				level1[i][j].type = TILE::GROUND;
			}
		}
	}
}
void Level2(tile level2[][cantmaxx]) {
	for (short i = 0; i < cantmaxy; i++) {
		for (short j = 0; j < cantmaxx; j++) {
			if ((i == 0 || i == cantmaxy - 1) || (j == 0 || j == cantmaxx - 1)) {
				level2[i][j].type = TILE::WALL;
			}
			else {
				level2[i][j].type = TILE::GROUND;
			}
		}
	}
	for (short i = 3; i < cantmaxx-3; i++) {
		level2[(cantmaxy-1)/2][i].type= TILE::WALL;
	}
	level2[1][(cantmaxx - 1) / 2].type = TILE::GROUNDBOX;
	level2[cantmaxy-2][(cantmaxx - 1) / 2].type = TILE::GROUNDBOX;
	level2[((cantmaxy - 1) / 2) + 1][(cantmaxx - 1) / 2].type = TILE::BOX;
	level2[((cantmaxy - 1) / 2) - 1][(cantmaxx - 1) / 2].type = TILE::BOX;
}
void Level3(tile level3[][cantmaxx]) {
	for (short i = 0; i < cantmaxy; i++) {
		for (short j = 0; j < cantmaxx; j++) {
			if ((i == 0 || i == cantmaxy - 1) || (j == 0 || j == cantmaxx - 1)|| (i == 1)||(i==2)) {
				level3[i][j].type = TILE::WALL;
			}
			else {
				level3[i][j].type = TILE::GROUND;
			}
		}
	}

	level3[3][1].type = TILE::WALL;
	level3[3][2].type = TILE::WALL;
	level3[3][5].type = TILE::WALL;
	level3[3][6].type = TILE::WALL;
	level3[3][7].type = TILE::WALL;
	level3[3][8].type = TILE::WALL;
	level3[3][9].type = TILE::WALL;

	level3[4][1].type = TILE::WALL;
	level3[4][2].type = TILE::WALL;
	level3[4][4].type = TILE::BOX;
	level3[4][5].type = TILE::WALL;
	level3[4][6].type = TILE::WALL;
	level3[4][7].type = TILE::WALL;
	level3[4][9].type = TILE::WALL;

	level3[5][1].type = TILE::WALL;
	level3[5][2].type = TILE::WALL;
	level3[5][7].type = TILE::BOX;

	level3[7][2].type = TILE::BOX;
	level3[7][4].type = TILE::GROUNDBOX;

	level3[8][2].type = TILE::GROUNDBOX;
	level3[8][5].type = TILE::GROUNDBOX;
}
void Level4(tile level4[][cantmaxx]) {
	for (short i = 0; i < cantmaxy; i++) {
		for (short j = 0; j < cantmaxx; j++) {
			if ((i == 0 || i == cantmaxy - 1) || (j == 0 || j == cantmaxx - 1) || (i == 1) || (i == 2)) {
				level4[i][j].type = TILE::WALL;
			}
			else {
				level4[i][j].type = TILE::GROUND;
			}
		}
	}
	level4[4][1].type = TILE::WALL;
	level4[5][1].type = TILE::GROUNDBOX;
	level4[4][2].type = TILE::BOX;
	level4[5][4].type = TILE::WALL;
	level4[5][5].type = TILE::WALL;
	level4[5][6].type = TILE::WALL;
	level4[6][1].type = TILE::WALL;
	level4[6][8].type = TILE::WALL;
	level4[7][8].type = TILE::GROUNDBOX;
	level4[6][7].type = TILE::BOX;
	level4[8][8].type = TILE::WALL;
	level4[8][2].type = TILE::WALL;
	level4[8][3].type = TILE::GROUNDBOX;
	level4[8][4].type = TILE::WALL;
	level4[7][4].type = TILE::BOX;

}
void Level5(tile level5[][cantmaxx]) {
	for (short i = 0; i < cantmaxy; i++) {
		for (short j = 0; j < cantmaxx; j++) {
			if ((i == 0 || i == cantmaxy - 1) || (j == 0 || j == cantmaxx - 1) || (i == 1) || (i == 2)) {
				level5[i][j].type = TILE::WALL;
			}
			else {
				level5[i][j].type = TILE::GROUND;
			}
		}
	}
	level5[3][1].type = TILE::GROUNDBOX;
	level5[3][6].type = TILE::GROUNDBOX;
	level5[4][1].type = TILE::WALL;
	level5[5][1].type = TILE::GROUNDBOX;
	level5[4][2].type = TILE::BOX;
	level5[5][4].type = TILE::WALL;
	level5[5][5].type = TILE::WALL;
	level5[5][6].type = TILE::WALL;
	level5[6][1].type = TILE::WALL;
	level5[6][8].type = TILE::WALL;
	level5[7][8].type = TILE::GROUNDBOX;
	level5[6][7].type = TILE::BOX;
	level5[8][8].type = TILE::WALL;
	level5[8][2].type = TILE::WALL;
	level5[8][4].type = TILE::WALL;
	level5[7][4].type = TILE::BOX;
}