#ifndef CANTLEVELS_H
#define CANTLEVELS_H
#include "raylib.h"
enum class TILE { GROUND, GROUNDBOX, BOX, WALL,PLAYER };
const short cantmaxx = 10;
const short cantmaxy = 10;
struct tile {
	TILE type;
	Rectangle boxcollider;
};
void Level1(tile level1[][cantmaxx]);
void Level2(tile level2[][cantmaxx]);
void Level3(tile level3[][cantmaxx]);
void Level4(tile level4[][cantmaxx]);
void Level5(tile level5[][cantmaxx]);
#endif // !CANTLEVELS_H
