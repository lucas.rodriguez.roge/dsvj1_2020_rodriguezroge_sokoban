#include"Game.h"
void Game::init(short input) {
	PlayMusicStream(sound::musicgame);
	_gameover = false;
	_cantboxes = 0;
	_ground = LoadTexture("res/Textures/Enviroment/ground.png");
	_groundbox = LoadTexture("res/Textures/Enviroment/groundbox.png");
	_wall = LoadTexture("res/Textures/Enviroment/wall.png");
	_replay = LoadTexture("res/Textures/replay.png");
	_exit = LoadTexture("res/Textures/goback.png");
	_box = LoadTexture("res/Textures/Enviroment/box.png");
	switch (input) {
	case 1:
		Level1(_grid);
		_player = new Player({ static_cast<float>(_ground.width) * 5,static_cast<float>(_ground.height) * 5 });
		_grid[5][5].type = TILE::PLAYER;
		break;
	case 2:
		Level2(_grid);
		_player = new Player({ static_cast<float>(_ground.width) * 5,static_cast<float>(_ground.height) * 5 });
		_grid[5][5].type = TILE::PLAYER;
		break;
	case 3:
		Level3(_grid);
		_player = new Player({ static_cast<float>(_ground.width) * 5,static_cast<float>(_ground.height) * 8 });
		_grid[5][8].type = TILE::PLAYER;
		break;
	case 4:
		Level4(_grid);
		_player = new Player({ static_cast<float>(_ground.width) * 4,static_cast<float>(_ground.height) * 7 });
		_grid[4][7].type = TILE::PLAYER;
		break;
	case 5:
		Level5(_grid);
		_player = new Player({ static_cast<float>(_ground.width) * 4,static_cast<float>(_ground.height) * 7 });
		_grid[4][7].type = TILE::PLAYER;
		break;
	}
	for (short i = 0; i < cantmaxy; i++) {
		for (short j = 0; j < cantmaxx; j++) {
			_grid[i][j].boxcollider.x = j * _ground.width;
			_grid[i][j].boxcollider.y = i * _ground.height;
			_grid[i][j].boxcollider.width = _ground.width;
			_grid[i][j].boxcollider.height = _ground.height;
		}
	}
	_miliseconds=0;
	_seconds=0;
	_minutes=0;
	_score = 10000;
	_player->setDirection(DIRECTION::FRONT);
}
void Game::update(short input) {
	UpdateMusicStream(sound::musicgame);
	playerMovement();
	timer();
	if (_score <= 0) {
		_gameover = true;
	}
	if (CheckCollisionPointRec(GetMousePosition(), { static_cast<float>(350), static_cast<float>(screenHeight - 40),static_cast<float>(_replay.width),static_cast<float>(_replay.height) })&&(IsMouseButtonPressed(MOUSE_LEFT_BUTTON))) {
		replayGame(input);
	}
	if (CheckCollisionPointRec(GetMousePosition(), { static_cast<float>(450), static_cast<float>(screenHeight - 40),static_cast<float>(_exit.width),static_cast<float>(_exit.height) }) && (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))) {
		_gameover = true;
		_score = 0;
	}
	if (!thereIsNoneGroundPosition()) {
		_gameover = true;
	}
}
void Game::draw() {
	drawGrid();
	ui();
}
void Game::reinit() {

}
void Game::play(short input,short& score) {
	init(input);
	while (!_gameover) {
		BeginDrawing();
		ClearBackground(WHITE);
		draw();
		update(input);
		EndDrawing();
	}
	reinit();
}
void Game::drawGrid() {
	short auxbox = 0;
	for (short i = 0; i < cantmaxy; i++) {
		for (short j = 0; j < cantmaxx; j++) {
			switch (_grid[i][j].type)
			{
			case TILE::GROUND:
				DrawTexture(_ground, _grid[i][j].boxcollider.x, _grid[i][j].boxcollider.y, WHITE);
				break;
			case TILE::WALL:
				DrawTexture(_wall ,_grid[i][j].boxcollider.x, _grid[i][j].boxcollider.y, WHITE);
				break;
			case TILE::GROUNDBOX:
				DrawTexture(_groundbox, _grid[i][j].boxcollider.x, _grid[i][j].boxcollider.y, WHITE);
				break;
			case TILE::BOX:
				DrawTexture(_box, _grid[i][j].boxcollider.x, _grid[i][j].boxcollider.y, WHITE);
				break;
			case TILE::PLAYER:
				DrawTexture(_ground, _grid[i][j].boxcollider.x, _grid[i][j].boxcollider.y, WHITE);
				_player->draw();
				break;
			default:
				break;
			}
		}
	}
}
void Game::playerMovement() {
	bool auxmovement = true;
	for (short i = 0; i < cantmaxy; i++) {
		for (short j = 0; j < cantmaxx; j++) {
			if (IsKeyPressed(KEY_UP) && (_grid[i-1][j].type != TILE::WALL) &&(_grid[i][j].type==TILE::PLAYER&&auxmovement)&& (_grid[i - 1][j].type != TILE::BOX) && (_grid[i-1][j].type != TILE::GROUNDBOX)) {
				_grid[i-1][j].type = TILE::PLAYER;
				_grid[i][j].type = TILE::GROUND;
				_player->setPos({ _grid[i-1][j].boxcollider.x,_grid[i-1][j].boxcollider.y });
				_player->setDirection(DIRECTION::BACK);
				auxmovement = false;
			}
			else if (IsKeyPressed(KEY_UP) && (_grid[i - 1][j].type != TILE::WALL) && (_grid[i][j].type == TILE::PLAYER && auxmovement)&&(_grid[i - 1][j].type == TILE::BOX)&& (_grid[i - 2][j].type != TILE::WALL)) {
				_grid[i - 1][j].type = TILE::PLAYER;
				_grid[i][j].type = TILE::GROUND;
				_player->setPos({ _grid[i - 1][j].boxcollider.x,_grid[i - 1][j].boxcollider.y });
				_player->setDirection(DIRECTION::BACK);
				_grid[i - 2][j].type = TILE::BOX;
				auxmovement = false;
			}
			if (IsKeyPressed(KEY_DOWN) && (_grid[i+1][j].type != TILE::WALL) && (_grid[i][j].type == TILE::PLAYER && auxmovement) && (_grid[i + 1][j].type != TILE::BOX) && (_grid[i+1][j].type != TILE::GROUNDBOX)) {
				_grid[i + 1][j].type = TILE::PLAYER;
				_grid[i][j].type = TILE::GROUND;
				_player->setPos({ _grid[i+1][j].boxcollider.x,_grid[i+1][j].boxcollider.y });
				_player->setDirection(DIRECTION::FRONT);
				auxmovement = false;
			}
			else if (IsKeyPressed(KEY_DOWN) && (_grid[i + 1][j].type != TILE::WALL) && (_grid[i][j].type == TILE::PLAYER && auxmovement) && (_grid[i + 1][j].type == TILE::BOX) && (_grid[i + 2][j].type != TILE::WALL)) {
				_grid[i + 1][j].type = TILE::PLAYER;
				_grid[i][j].type = TILE::GROUND;
				_player->setPos({ _grid[i + 1][j].boxcollider.x,_grid[i + 1][j].boxcollider.y });
				_player->setDirection(DIRECTION::FRONT);
				_grid[i + 2][j].type = TILE::BOX;
				auxmovement = false;
			}
			if (IsKeyPressed(KEY_LEFT) && (_grid[i][j - 1].type != TILE::WALL) && (_grid[i][j].type == TILE::PLAYER && auxmovement) && (_grid[i][j-1].type != TILE::BOX) && (_grid[i][j - 1].type != TILE::GROUNDBOX)) {
				_grid[i][j-1].type = TILE::PLAYER;
				_grid[i][j].type = TILE::GROUND;
				_player->setPos({ _grid[i][j-1].boxcollider.x,_grid[i][j-1].boxcollider.y });
				_player->setDirection(DIRECTION::LEFT);
				auxmovement = false;
			}
			else if (IsKeyPressed(KEY_LEFT) && (_grid[i][j - 1].type != TILE::WALL) && (_grid[i][j].type == TILE::PLAYER && auxmovement) && (_grid[i][j - 1].type == TILE::BOX) && (_grid[i][j - 2].type != TILE::WALL)) {
				_grid[i][j - 1].type = TILE::PLAYER;
				_grid[i][j].type = TILE::GROUND;
				_player->setPos({ _grid[i][j - 1].boxcollider.x,_grid[i][j - 1].boxcollider.y });
				_player->setDirection(DIRECTION::LEFT);
				_grid[i][j - 2].type = TILE::BOX;
				auxmovement = false;
			}
			if (IsKeyPressed(KEY_RIGHT) && (_grid[i][j+1].type!=TILE::WALL) && (_grid[i][j].type == TILE::PLAYER && auxmovement) && (_grid[i][j+1].type != TILE::BOX)&&(_grid[i][j + 1].type != TILE::GROUNDBOX)) {
				_grid[i][j + 1].type = TILE::PLAYER;
				_grid[i][j].type = TILE::GROUND;
				_player->setPos({ _grid[i][j+1].boxcollider.x,_grid[i][j+1].boxcollider.y });
				_player->setDirection(DIRECTION::RIGHT);
				auxmovement=false;
			}
			else if(IsKeyPressed(KEY_RIGHT) && (_grid[i][j + 1].type != TILE::WALL) && (_grid[i][j].type == TILE::PLAYER && auxmovement) && (_grid[i][j + 1].type == TILE::BOX) && (_grid[i][j + 2].type != TILE::WALL&& _grid[i][j + 2].type != TILE::BOX)) {
				_grid[i][j + 1].type = TILE::PLAYER;
				_grid[i][j].type = TILE::GROUND;
				_player->setPos({ _grid[i][j + 1].boxcollider.x,_grid[i][j + 1].boxcollider.y });
				_player->setDirection(DIRECTION::RIGHT);
				_grid[i][j + 2].type = TILE::BOX;
				auxmovement = false;
			}
		}
	}
}
bool Game::thereIsNoneGroundPosition() {
	bool auxwin = false;
	for (short i = 0; i < cantmaxy; i++) {
		for (short j = 0; j < cantmaxx; j++) {
			if (_grid[i][j].type == TILE::GROUNDBOX) {
				auxwin = true;
				break;
			}
		}
		if (auxwin)
			break;
	}
	return auxwin;
}
void Game::timer() {
	if (_miliseconds < 60) {
		_miliseconds++;
	}
	else {
		_miliseconds = 0;
		if (_seconds < 60) {
			_seconds++;
			_score -= 100;
		}
		else {
			_seconds = 0;
			_minutes++;
		}
	}
}
void Game::ui() {
	DrawText("TIMER: ", 0, screenHeight - 40, 40, BLACK);
	DrawText(FormatText("%d", _minutes), 160, screenHeight - 40, 40, BLACK);
	DrawText(":", 180, screenHeight - 40, 40, BLACK);
	DrawText(FormatText("%d", _seconds),  190, screenHeight - 40, 40, BLACK);
	DrawTexture(_replay, 350, screenHeight - 40, WHITE);
	DrawTexture(_exit, 450, screenHeight - 40, WHITE);
}
void Game::replayGame(short input) {
	delete _player;
	switch (input) {
	case 1:
		Level1(_grid);
		_player = new Player({ static_cast<float>(_ground.width) * 5,static_cast<float>(_ground.height) * 5 });
		_grid[5][5].type = TILE::PLAYER;
		break;
	case 2:
		Level2(_grid);
		_player = new Player({ static_cast<float>(_ground.width) * 5,static_cast<float>(_ground.height) * 5 });
		_grid[5][5].type = TILE::PLAYER;
		break;
	case 3:
		Level3(_grid);
		_player = new Player({ static_cast<float>(_ground.width) * 5,static_cast<float>(_ground.height) * 8 });
		_grid[5][8].type = TILE::PLAYER;
		break;
	case 4:
		Level4(_grid);
		_player = new Player({ static_cast<float>(_ground.width) * 4,static_cast<float>(_ground.height) * 7 });
		_grid[4][7].type = TILE::PLAYER;
		break;
	case 5:
		Level5(_grid);
		_player = new Player({ static_cast<float>(_ground.width) * 4,static_cast<float>(_ground.height) * 7 });
		_grid[4][7].type = TILE::PLAYER;
		break;
	}
	_player->setDirection(DIRECTION::FRONT);
}