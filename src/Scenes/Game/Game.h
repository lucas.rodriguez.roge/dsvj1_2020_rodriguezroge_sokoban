#pragma once
#ifndef GAME_H
#define GAME_H

#include "../Scene.h"
#include "raylib.h"
#include "../../Entities/Box/Box.h"
#include "../../Entities/Buttons/Buttons.h"
#include "../../Entities/Player/Player.h"
#include "../../Entities/Sound/Sound.h"
#include "CantLevels.h"
const short cantmaxbox=10;
enum class POSITION{LEFT,RIGHT,UP,DOWN};
class Game{
private:
	Texture2D _ground;
	Texture2D _groundbox;
	Texture2D _wall;
	Texture2D _replay;
	Texture2D _exit;
	Texture2D _box;
	short _cantboxes;
	Player* _player;
	tile _grid[cantmaxy][cantmaxx];
	bool _gameover;
	short _miliseconds;
	short _seconds;
	short _minutes;
	short _score;
public:
	void init(short input);
	void update(short input);
	void draw();
	void reinit();
	void play(short input,short& score);
	void playerMovement();
	void drawGrid();
	bool thereIsNoneGroundPosition();
	void timer();
	void ui();
	void replayGame(short input);
};
#endif // !GAME_H

