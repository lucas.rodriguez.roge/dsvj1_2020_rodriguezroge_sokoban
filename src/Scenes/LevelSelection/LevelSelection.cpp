#include "LevelSelection.h"  
void LevelSelection::init() {
    _alltextures[0] = LoadTexture("res/Textures/minitextbox.png");
    _alltextures[1] = LoadTexture("res/Textures/textbox.png");
    _alltextures[2] = LoadTexture("res/Textures/levelselectionbackground.png");
    PlayMusicStream(sound::musiclevelselection);
    for (short i = 0;i < MAX_LEVELS;i++) {
        options[i] = new Buttons();
        switch (i) {
        case 0:
            options[i]->init({ ((float)i * 100),300 }, TYPE::LEVEL1, _alltextures[0]);
            break;
        case 1:
            options[i]->init({ ((float)i * 100),300 }, TYPE::LEVEL2, _alltextures[0]);
            break;
        case 2:
            options[i]->init({ ((float)i * 100),300 }, TYPE::LEVEL3, _alltextures[0]);
            break;
        case 3:
            options[i]->init({ ((float)i * 100),300 }, TYPE::LEVEL4, _alltextures[0]);
            break;
        case 4:
            options[i]->init({ ((float)i * 100),300 }, TYPE::LEVEL5, _alltextures[0]);
            break;
        case 5:
            options[i]->init({ 25,450 },TYPE::GOBACK, _alltextures[1]);
            break;
        }
    }
    backmenu = false;
}
void LevelSelection::update(short & score) {
    UpdateMusicStream(sound::musiclevelselection);
    for (short i = 0; i < MAX_LEVELS; i++) {
        options[i]->buttonManagment();
    }
    if (CheckCollisionPointRec(GetMousePosition(), options[0]->getBoxCollider()) && (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))) {
        Game* level1 = new Game();
        level1->play(1,score);
        delete level1;
    }
    if (CheckCollisionPointRec(GetMousePosition(), options[1]->getBoxCollider()) && (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))) {
        Game* level2 = new Game();
        level2->play(2,score);
        delete level2;
    }
    if (CheckCollisionPointRec(GetMousePosition(), options[2]->getBoxCollider()) && (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))) {
        Game* level3 = new Game();
        level3->play(3,score);
        delete level3;
    }
    if (CheckCollisionPointRec(GetMousePosition(), options[3]->getBoxCollider()) && (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))) {
        Game* level4 = new Game();
        level4->play(4,score);
        delete level4;
    }
    if (CheckCollisionPointRec(GetMousePosition(), options[4]->getBoxCollider()) && (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))) {
        Game* level5 = new Game();
        level5->play(5,score);
        delete level5;
    }
    if (CheckCollisionPointRec(GetMousePosition(), options[5]->getBoxCollider()) && (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))) {
        backmenu = true;
    }
}
void LevelSelection::draw() {
    BeginDrawing();
    ClearBackground(BLACK);
    DrawTexture(_alltextures[2], 0, 0, WHITE);
    for (short i = 0;i < MAX_LEVELS;i++) {
        options[i]->draw();
    }
    EndDrawing();
}
void LevelSelection::play(short& score) {
    init();
    while (backmenu==false) {
        update(score);
        draw();
    }
}
