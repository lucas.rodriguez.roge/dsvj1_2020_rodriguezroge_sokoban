#pragma once
#ifndef LEVELSELECION_H
#define LEVELSELECTION_H
#define MAX_LEVELS 6
#include "../../Entities/Buttons/Buttons.h"
#include "../Game/Game.h"
#include "../MainMenu/MainMenu.h"
const short canttextureslevels = 3;
class LevelSelection {
private:
	Buttons* options[MAX_LEVELS];
	Texture2D _alltextures[canttextureslevels];
	TYPE _input;
	bool backmenu;
public:
	void init();
	void update(short& score);
	void draw();
	void play(short& score);
};
#endif // !LEVELSELECION_H
