#include "MainMenu.h"
void MainMenu::init() {
	sound::initSounds();
	PlayMusicStream(sound::musicmenu);
	_alltextures[0] = LoadTexture("res/Textures/mainmenubackground.png");
	_alltextures[1] = LoadTexture("res/Textures/textbox.png");
	_alltextures[2] = LoadTexture("res/Textures/title.png");
	_background=_alltextures[0];
	Vector2 playpos;
	Vector2 optionspos;
	Vector2 exitpos;
	TYPE play=TYPE::PLAY;
	TYPE options=TYPE::OPTIONS;
	TYPE exit = TYPE::EXIT;
	playpos.x = screenWidth / 2- (_alltextures[1].width / 2);
	playpos.y = screenHeight / 3;
	optionspos.x = playpos.x;
	optionspos.y = screenHeight / 2;
	exitpos.x = playpos.x;
	exitpos.y = screenHeight /1.5;

	for (short i = 0; i < cantbotones; i++) {
		_buttons[i] = new Buttons();
	}
	_buttons[0]->init(playpos, play, _alltextures[1]);
	_buttons[1]->init(optionspos, options, _alltextures[1]);
	_buttons[2]->init(exitpos, exit, _alltextures[1]);
	_exit = false;
	score = 0;
}
void MainMenu::update() {
	UpdateMusicStream(sound::musicmenu);
	for (short i = 0; i < cantbotones; i++) {
		_buttons[i]->buttonManagment();
	}
	if (CheckCollisionPointRec(GetMousePosition(), _buttons[0]->getBoxCollider())&&(IsMouseButtonReleased(MOUSE_LEFT_BUTTON))) {
		LevelSelection* levelselection = new LevelSelection();
		levelselection->play(score);
		delete levelselection;
	}
	if (CheckCollisionPointRec(GetMousePosition(), _buttons[1]->getBoxCollider()) && (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))) {
		OptionsMenu* optionsmenu = new OptionsMenu();
		optionsmenu->play(score);
		delete optionsmenu;
	}
	if (CheckCollisionPointRec(GetMousePosition(), _buttons[2]->getBoxCollider()) && (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))) {
		_exit = true;
	}
	if (score > maxscore) {
		maxscore = score;
		score = 0;
	}
}
void MainMenu::draw() {
	DrawTexture(_background, 0, 0, WHITE);
	DrawTexture(_alltextures[2], 50, 30, WHITE);
	for (short i = 0;i < cantbotones; i++)
	{
		_buttons[i]->draw();
	}
	DrawText("V0.7", 0, screenHeight-20, 20, WHITE);
	if (CheckCollisionPointRec(GetMousePosition(), _buttons[0]->getBoxCollider())) {
		DrawText("MaxScore: ", _buttons[0]->getBoxCollider().x, _buttons[0]->getBoxCollider().y - 50, 40, YELLOW);
		DrawText(FormatText("%d", maxscore), _buttons[0]->getBoxCollider().x + 225, _buttons[0]->getBoxCollider().y - 50, 40, RED);
	}
}
void MainMenu::reinit() {
	for (short i = 0; i < _canttexturesoptions; i++) {
		UnloadTexture(_alltextures[i]);
	}
}
void MainMenu::play() {
	init();
	while (!WindowShouldClose()&&!_exit) {
		BeginDrawing();
		ClearBackground(BLACK);
		update();
		draw();
		EndDrawing();
	}
	reinit();
}