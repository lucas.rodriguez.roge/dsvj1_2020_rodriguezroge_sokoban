#ifndef MAINMENU_H
#define MAINMENU_H
#include "../../Entities/Buttons/Buttons.h"
#include "../../Entities/Sound/Sound.h"
#include "../LevelSelection/LevelSelection.h"
#include "../OptionsMenu/OptionsMenu.h"
#include "raylib.h"
const short cantbotones = 3;
const short _canttexturesoptions = 3;
class MainMenu{
private:
	Texture2D _alltextures[_canttexturesoptions];
	Buttons* _buttons[cantbotones];
	bool _exit;
	short score;
	short maxscore;
	Texture2D _background;
public:
	void init();
	void update();
	void draw();
	void reinit();
	void play();
};
#endif // !MAINMENU_H
