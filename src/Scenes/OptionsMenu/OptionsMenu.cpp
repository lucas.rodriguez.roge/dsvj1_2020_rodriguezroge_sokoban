#include "OptionsMenu.h"
void OptionsMenu::init() {
	_alltextures[0] = LoadTexture("res/Textures/optionsbackground.png");
	_alltextures[1] = LoadTexture("res/Textures/textbox.png");
	PlayMusicStream(sound::musicoptions);
	Vector2 creditspos;
	Vector2 volumepos;
	Vector2 gobackpos;

	TYPE credits = TYPE::CREDITS;
	TYPE volume = TYPE::VOLUME;
	TYPE goback = TYPE::GOBACK;

	creditspos.x = screenWidth / 2 - (_alltextures[1].width / 2);
	creditspos.y = screenHeight / 3;
	volumepos.x = screenWidth / 2 - (_alltextures[1].width / 2);
	volumepos.y = screenHeight / 2;
	gobackpos.x = screenWidth / 2 - (_alltextures[1].width / 2);
	gobackpos.y = screenHeight - 100;
	for (short i = 0; i < cantbotonesoptions; i++) {
		_buttons[i] = new Buttons();
	}
	_buttons[0]->init(creditspos, credits, _alltextures[1]);
	_buttons[1]->init(volumepos, volume, _alltextures[1]);
	_buttons[2]->init(gobackpos, goback, _alltextures[1]);
	_exit = false;
}
void OptionsMenu::update(short& score) {
	UpdateMusicStream(sound::musicoptions);
	for (short i = 0; i < cantbotonesoptions; i++) {
		_buttons[i]->buttonManagment();
	}
	if (CheckCollisionPointRec(GetMousePosition(), _buttons[1]->getBoxCollider()) && (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))) {
		VolumeMenu* volumemenu = new VolumeMenu();
		volumemenu->play(score);
		delete volumemenu;
	}
	if (CheckCollisionPointRec(GetMousePosition(), _buttons[2]->getBoxCollider()) && (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))) {
		_exit = true;
	}
}
void OptionsMenu::draw() {
	DrawTexture(_alltextures[0], 0, 0, WHITE);
	for (short i = 0; i < cantbotonesoptions; i++)
	{
		_buttons[i]->draw();
	}
	if (CheckCollisionPointRec(GetMousePosition(), _buttons[0]->getBoxCollider())) {
		DrawText("MADE BY GENTLEMEN DOGGO", 0, screenHeight-20, 20, WHITE);
	}
}
void OptionsMenu::reinit() {
	for (short i = 0; i < _canttextures; i++) {
		UnloadTexture(_alltextures[i]);
	}
}
void OptionsMenu::play(short& score) {
	init();
	while (!_exit) {
		BeginDrawing();
		ClearBackground(BLACK);
		update(score);
		draw();
		EndDrawing();
	}
	reinit();
}
