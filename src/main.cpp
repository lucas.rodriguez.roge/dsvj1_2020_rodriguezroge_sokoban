
#include "Config.h"
#include "raylib.h"
#include "Scenes/MainMenu/MainMenu.h"
int main(void)
{
    InitWindow(screenWidth, screenHeight, "SOKOBAN-BY GAIUS");
    InitAudioDevice();
    MainMenu* mainmenu = new MainMenu();
    SetTargetFPS(60);              
    mainmenu->play();
    delete mainmenu;
    CloseAudioDevice();
}